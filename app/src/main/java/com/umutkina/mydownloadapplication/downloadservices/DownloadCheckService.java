package com.umutkina.mydownloadapplication.downloadservices;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;

import com.mozillaonline.providers.DownloadManager;
import com.squareup.otto.Produce;
import com.umutkina.mydownloadapplication.DowloadApplication;
import com.umutkina.mydownloadapplication.modals.DownlodInfo;
import com.umutkina.mydownloadapplication.otto.BusStation;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by umutkina on 09/06/16.
 */
public class DownloadCheckService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    startJob();
                }
            }
        });
        t.start();
        return START_STICKY;
    }

    private void startJob() {
        //do job here
        BusStation.getMainThreadBus().post("deneme");

        DownloadManager mDownloadManager = new DownloadManager(getContentResolver(),
                getPackageName());
        mDownloadManager.setAccessAllDownloads(true);

        DownloadManager.Query myDownloadQuery = new DownloadManager.Query();

        Cursor cursor = mDownloadManager.query(myDownloadQuery);

        ArrayList<Integer> statuses = new ArrayList<>();
//        startManagingCursor(cursor);
//                cursor.moveToFirst();
        try {
            while (cursor.moveToNext()) {
                int mIdColumnId = cursor.getColumnIndexOrThrow(DownloadManager.COLUMN_ID);

                int mIdStatus = cursor.getColumnIndexOrThrow(DownloadManager.COLUMN_STATUS);

                int mTotalBytesColumnId = cursor
                        .getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
                int mCurrentBytesColumnId = cursor
                        .getColumnIndexOrThrow(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
                int mDescrColumnId = cursor
                        .getColumnIndexOrThrow(DownloadManager.COLUMN_DESCRIPTION);

                long totalBytes = cursor.getLong(mTotalBytesColumnId);
                String desc = cursor.getString(mDescrColumnId);


                long currentBytes = cursor.getLong(mCurrentBytesColumnId);
                long downloadId = cursor.getLong(mIdColumnId);
                int status = cursor.getInt(mIdStatus);
                statuses.add(status);
                if (!(status == DownloadManager.STATUS_FAILED || status == DownloadManager.STATUS_SUCCESSFUL)) {
                    DowloadApplication application = (DowloadApplication) getApplicationContext();
                    HashMap<String, DownlodInfo> downlodInfoHashMap = application.getDownlodInfoHashMap();

                    DownlodInfo downlodInfo = new DownlodInfo();

                    downlodInfo.setCurrentProgress(currentBytes);
                    downlodInfo.setTotalProgress(totalBytes);
                    downlodInfo.setDescription(desc);
                    downlodInfo.setStatus(status);
                    downlodInfo.setDownloadId(downloadId);
                    downlodInfoHashMap.put(desc, downlodInfo);

                    application.setDownlodInfoHashMap(downlodInfoHashMap);
                }


//                if (title.equalsIgnoreCase(s)) {
//                    Toast.makeText(getApplicationContext(), currentBytes + " / " + totalBytes, Toast.LENGTH_SHORT).show();
//
//                }

            }
        } finally {
            cursor.close();
        }

        int counter = 0;
        for (Integer status : statuses) {
            if (!(status == DownloadManager.STATUS_FAILED || status == DownloadManager.STATUS_SUCCESSFUL)) {
                counter++;
            }

        }
        if (counter==statuses.size()) {
            stopSelf();
        }
//        DowloadApplication application = (DowloadApplication) getApplicationContext();
//        HashMap<String, DownlodInfo> downlodInfoHashMap = application.getDownlodInfoHashMap();

        //job completed. Rest for 5 second before doing another one
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //do job again

    }

    @Produce
    public String produceEvent() {
        return "Starting up";
    }
}