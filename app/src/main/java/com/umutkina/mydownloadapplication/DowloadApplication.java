package com.umutkina.mydownloadapplication;

import android.app.Application;

import com.umutkina.mydownloadapplication.modals.DownlodInfo;

import java.util.HashMap;

/**
 * Created by umutkina on 07/06/16.
 */
public class DowloadApplication extends Application {

    HashMap<String ,DownlodInfo> downlodInfoHashMap= new HashMap<String, DownlodInfo>();
    @Override
    public void onCreate() {
        super.onCreate();


    }

    long currentDownloadId;

    public long getCurrentDownloadId() {
        return currentDownloadId;
    }

    public void setCurrentDownloadId(long currentDownloadId) {
        this.currentDownloadId = currentDownloadId;
    }

    public HashMap<String, DownlodInfo> getDownlodInfoHashMap() {
        return downlodInfoHashMap;
    }

    public void setDownlodInfoHashMap(HashMap<String, DownlodInfo> downlodInfoHashMap) {
        this.downlodInfoHashMap = downlodInfoHashMap;
    }
}
