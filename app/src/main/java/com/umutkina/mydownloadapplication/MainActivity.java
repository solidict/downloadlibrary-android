package com.umutkina.mydownloadapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mozillaonline.providers.DownloadManager;
import com.mozillaonline.providers.downloads.DownloadService;
import com.squareup.otto.Subscribe;
import com.umutkina.mydownloadapplication.downloadservices.DownloadCheckService;
import com.umutkina.mydownloadapplication.modals.DownlodInfo;
import com.umutkina.mydownloadapplication.otto.*;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity {
    DownloadManager mDownloadManager;
    private BroadcastReceiver mReceiver;

     DowloadApplication dowloadApplication;
    String id="1212213";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvDownload= (TextView) findViewById(R.id.tv_download);
        TextView tvCheckStatus= (TextView) findViewById(R.id.tv_check_status);
        dowloadApplication = (DowloadApplication) getApplication();

        BusStation.getMainThreadBus().register(this);
        tvDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://down.mumayi.com/41052/mbaidu";

                String name="deneme";
                startDownload(url,id,name);
            }
        });
        tvCheckStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();

                DownlodInfo downlodInfo = downlodInfoHashMap.get(id);
                if (downlodInfo != null) {
                    Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();

                }
            }
        });

        mDownloadManager = new DownloadManager(getContentResolver(),
                getPackageName());
        startDownloadService();


        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
//               TODO statusbardaki indirme progres view'ına tıklayınca ne olacağı
            }


        };

        registerReceiver(mReceiver, new IntentFilter(
                DownloadManager.ACTION_NOTIFICATION_CLICKED));
    }
        public void checkDownload() {


        Intent msgIntent = new Intent(this, DownloadCheckService.class);
//		msgIntent.putExtra(SimpleIntentService.PARAM_IN_MSG, strInputMsg);
        startService(msgIntent);
    }

    @Subscribe
    public void getMessage(String s) {
//        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

        HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
        DownlodInfo downlodInfo = downlodInfoHashMap.get(id);
        if (downlodInfo != null) {
            Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();

        }
    }


    private void startDownloadService() {
        Intent intent = new Intent();
        intent.setClass(this, DownloadService.class);
        startService(intent);
    }
    private void startDownload(String url ,String id,String name) {
        checkDownload();
        Uri srcUri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(srcUri);
        request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS, "/");
        request.setTitle(name);
        request.setDescription(id);
        long enqueue = mDownloadManager.enqueue(request);

        DowloadApplication dowloadApplication = (DowloadApplication) getApplication();
        dowloadApplication.setCurrentDownloadId(enqueue);


    }

    private void pauseDownload(Context context, String id) {
        DowloadApplication dowloadApplication= (DowloadApplication) context.getApplicationContext();
        DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
        HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
        String key = id;
        DownlodInfo downlodInfo = downlodInfoHashMap.get(key);
        if (downlodInfo != null) {
            myDownloadQuery.setFilterById(downlodInfo.getDownloadId());
            Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();
        }

        Cursor cursor = mDownloadManager.query(myDownloadQuery);
//        startManagingCursor(cursor);
        if (cursor.moveToFirst()) {
            // TODO yukarıdaki kodların hepsi  o idli bir request var mı diye kontrol etmek için ,gereksiz olabilir tümünü silip denemek lazım,

            mDownloadManager.resumeDownload(downlodInfo.getDownloadId());
        }
        cursor.close();
    }

    private void downloadResume(Context  context, String id) {
        DowloadApplication dowloadApplication= (DowloadApplication) context.getApplicationContext();
        DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
        HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
        String key = id;
        DownlodInfo downlodInfo = downlodInfoHashMap.get(key);
        if (downlodInfo != null) {
            myDownloadQuery.setFilterById(downlodInfo.getDownloadId());
            Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();

        }


        Cursor cursor = mDownloadManager.query(myDownloadQuery);
        //startManagingCursor(cursor);

        if (cursor.moveToFirst()) {


            // TODO yukarıdaki kodların hepsi  o idli bir request var mı diye kontrol etmek için ,gereksiz olabilir tümünü silip denemek lazım,



            mDownloadManager.pauseDownload(downlodInfo.getDownloadId());
        }

        cursor.close();
    }

    private void cancelDownload(Context context, String id) {
        DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
        DowloadApplication dowloadApplication= (DowloadApplication) context.getApplicationContext();
        HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
        String key = id;
        DownlodInfo downlodInfo = downlodInfoHashMap.get(key);
        if (downlodInfo != null) {
            myDownloadQuery.setFilterById(downlodInfo.getDownloadId());
            Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();
        }

        Cursor cursor = mDownloadManager.query(myDownloadQuery);
//        startManagingCursor(cursor);
        if (cursor.moveToFirst()) {

            // TODO yukarıdaki kodların hepsi  o idli bir request var mı diye kontrol etmek için ,gereksiz olabilir tümünü silip denemek lazım,

            mDownloadManager.remove(downlodInfo.getDownloadId());
        }
        cursor.close();
    }



    protected void onDestroy() {
        unregisterReceiver(mReceiver);
        BusStation.getMainThreadBus().unregister(this);
        super.onDestroy();
    }
}
