package com.umutkina.mydownloadapplication.otto;

/**
 * Created by umutkina on 14/06/16.
 */
public class BusStation {
    public  static MainThreadBus mainThreadBus= new MainThreadBus();

    public static MainThreadBus getMainThreadBus() {
        return mainThreadBus;
    }

    public static void setMainThreadBus(MainThreadBus mainThreadBus) {
        BusStation.mainThreadBus = mainThreadBus;
    }
}
