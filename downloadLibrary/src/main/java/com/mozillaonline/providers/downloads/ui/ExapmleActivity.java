//package com.mozillaonline.providers.downloads.ui;
//
//import android.app.Activity;
//import android.database.ContentObserver;
//import android.database.Cursor;
//import android.os.Bundle;
//import android.os.Handler;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.mozillaonline.DowloadApplication;
//import com.mozillaonline.downloadprovider.R;
//import com.mozillaonline.modals.DownlodInfo;
//import com.mozillaonline.providers.DownloadManager;
//
//import java.util.HashMap;
//
//public class ExapmleActivity extends Activity {
//    private DownloadManager mDownloadManager;
//    private MyContentObserver mContentObserver = new MyContentObserver();
//    //    private MyDataSetObserver mDataSetObserver = new MyDataSetObserver();
//    Cursor cursor;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_exapmle);
//
//
//        mDownloadManager = new DownloadManager(getContentResolver(),
//                getPackageName());
//        mDownloadManager.setAccessAllDownloads(true);
//
//
//        //set the query filter to our previously Enqueued download
//        final DowloadApplication dowloadApplication = (DowloadApplication) getApplication();
////        final long currentDownloadId = dowloadApplication.getCurrentDownloadId();
////        myDownloadQuery.setFilterById(currentDownloadId);
//
//        //Query the download manager about downloads that have been requested.
//
//
//        Button button = (Button) findViewById(R.id.btn_status);
//
//        final EditText etName = (EditText) findViewById(R.id.et_name);
//        final Button buttonPause = (Button) findViewById(R.id.btn_pause);
//        Button buttonCancel = (Button) findViewById(R.id.btn_cancel);
//
//
//        buttonPause.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (buttonPause.getText().toString().equalsIgnoreCase("pause")) {
//                    DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
//                    HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
//                    String key = etName.getText().toString();
//                    DownlodInfo downlodInfo = downlodInfoHashMap.get(key);
//                    if (downlodInfo != null) {
//                        myDownloadQuery.setFilterById(downlodInfo.getDownloadId());
//                        Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();
//
//                    }
//
//
//
//                    cursor = mDownloadManager.query(myDownloadQuery);
//                    startManagingCursor(cursor);
//
//                    if (cursor.moveToFirst()) {
//                        buttonPause.setText("resume");
//
//
//
//                        mDownloadManager.pauseDownload(downlodInfo.getDownloadId());
//                    }
//
//
//                } else {
//                    DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
//                    HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
//                    String key = etName.getText().toString();
//                    DownlodInfo downlodInfo = downlodInfoHashMap.get(key);
//                    if (downlodInfo != null) {
//                        myDownloadQuery.setFilterById(downlodInfo.getDownloadId());
//                        Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    cursor = mDownloadManager.query(myDownloadQuery);
//                    startManagingCursor(cursor);
//                    if (cursor.moveToFirst()) {
//
//                        buttonPause.setText("pause");
//                        mDownloadManager.resumeDownload(downlodInfo.getDownloadId());
//                    }
//
//                }
//
//
//            }
//        });
//
//        buttonCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                DownloadManager.Query myDownloadQuery = new DownloadManager.Query();
//                HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
//                String key = etName.getText().toString();
//                DownlodInfo downlodInfo = downlodInfoHashMap.get(key);
//                if (downlodInfo != null) {
//                    myDownloadQuery.setFilterById(downlodInfo.getDownloadId());
//                    Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();
//                }
//
//                cursor = mDownloadManager.query(myDownloadQuery);
//                startManagingCursor(cursor);
//                if (cursor.moveToFirst()) {
//
//
//                    mDownloadManager.remove(downlodInfo.getDownloadId());
//                }
//            }
//        });
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                HashMap<String, DownlodInfo> downlodInfoHashMap = dowloadApplication.getDownlodInfoHashMap();
//                String key = etName.getText().toString();
//                DownlodInfo downlodInfo = downlodInfoHashMap.get(key);
//                if (downlodInfo != null) {
//                    Toast.makeText(getApplicationContext(), downlodInfo.getCurrentProgress() + " / " + downlodInfo.getTotalProgress(), Toast.LENGTH_SHORT).show();
//
//                }
//
////            cursor = mDownloadManager.query(myDownloadQuery);
////                startManagingCursor(cursor);
//////                cursor.moveToFirst();
////                try {
////                    while (cursor.moveToNext()) {
////                        int mTotalBytesColumnId = cursor
////                                .getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
////                        int mCurrentBytesColumnId = cursor
////                                .getColumnIndexOrThrow(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
////                        int mTitleColumnId = cursor
////                                .getColumnIndexOrThrow(DownloadManager.COLUMN_TITLE);
////
////                        long totalBytes = cursor.getLong(mTotalBytesColumnId);
////                        String title = cursor.getString(mTitleColumnId);
////
////                        String s = etName.getText().toString();
////                        long currentBytes = cursor.getLong(mCurrentBytesColumnId);
////
////
////                        if (title.equalsIgnoreCase(s)) {
////                            Toast.makeText(getApplicationContext(), currentBytes + " / " + totalBytes, Toast.LENGTH_SHORT).show();
////
////                        }
////
////                    }
////                } catch (Exception e){
////
////                    e.printStackTrace();
////                }
////
//
//            }
//        });
//    }
//
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//
////        fileObserver.startWatching();
////        cursor = mDownloadManager.query(myDownloadQuery);
////        cursor.registerContentObserver(mContentObserver);
////        Uri myDownloads = Uri.parse( "content://downloads/my_downloads" );
//
////        getContentResolver().registerContentObserver( myDownloads, true, mContentObserver );
//
////        cursor.registerDataSetObserver(mDataSetObserver);
////        cursor.requery();
//
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
////        fileObserver.stopWatching();
////        cursor.unregisterContentObserver(mContentObserver);
////        cursor.unregisterDataSetObserver(mDataSetObserver);
//    }
//
//    private class MyContentObserver extends ContentObserver {
//        public MyContentObserver() {
//            super(new Handler());
//        }
//
//        @Override
//        public void onChange(boolean selfChange) {
//
//            startManagingCursor(cursor);
//            if (cursor.moveToFirst()) {
//
//                int mTotalBytesColumnId = cursor
//                        .getColumnIndexOrThrow(DownloadManager.COLUMN_TOTAL_SIZE_BYTES);
//                int mCurrentBytesColumnId = cursor
//                        .getColumnIndexOrThrow(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR);
//
//                long totalBytes = cursor.getLong(mTotalBytesColumnId);
//                long currentBytes = cursor.getLong(mCurrentBytesColumnId);
//
//                Toast.makeText(getApplicationContext(), currentBytes + " / " + totalBytes, Toast.LENGTH_SHORT).show();
//            }
//
//        }
//    }
//
//
//}
