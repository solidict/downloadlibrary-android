//package com.mozillaonline.downloadprovider;
//
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.net.Uri;
//import android.os.Bundle;
//import android.os.Environment;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.EditText;
//
//
//import com.mozillaonline.providers.DownloadManager;
//import com.mozillaonline.providers.DownloadManager.Request;
//import com.mozillaonline.providers.downloads.DownloadService;
//import com.mozillaonline.providers.downloads.ui.DownloadList;
//import com.mozillaonline.providers.downloads.ui.ExapmleActivity;
//
//public class DownloadProviderActivity extends Activity implements
//        OnClickListener {
//    @SuppressWarnings("unused")
//    private static final String TAG = DownloadProviderActivity.class.getName();
//
//    private BroadcastReceiver mReceiver;
//
//    EditText mUrlInputEditText;
//    EditText etName;
//    Button mStartDownloadButton;
//    DownloadManager mDownloadManager;
//    Button mShowDownloadListButton;
//
//    /**
//     * Called when the activity is first created.
//     */
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.main);
//        sendLocation();
//        mDownloadManager = new DownloadManager(getContentResolver(),
//                getPackageName());
//        buildComponents();
//        startDownloadService();
//
//        mReceiver = new BroadcastReceiver() {
//
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                showDownloadList();
//            }
//        };
//
//        registerReceiver(mReceiver, new IntentFilter(
//                DownloadManager.ACTION_NOTIFICATION_CLICKED));
//    }
//
//    public void sendLocation() {
//        // Construct an intent that will execute the AlarmReceiver
////		Intent intent = new Intent(getApplicationContext(), DownloadCheckReceiver.class);
////		// Create a PendingIntent to be triggered when the alarm goes off
////		final PendingIntent pIntent = PendingIntent.getBroadcast(this, DownloadCheckReceiver.REQUEST_CODE,
////				intent, PendingIntent.FLAG_UPDATE_CURRENT);
////		// Setup periodic alarm every 5 seconds
////		long firstMillis = System.currentTimeMillis(); // alarm is set right away
////		AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
////		// First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
////		// Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
////		alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
////				3000, pIntent);
//
//        Intent msgIntent = new Intent(this, DownloadCheckService.class);
////		msgIntent.putExtra(SimpleIntentService.PARAM_IN_MSG, strInputMsg);
//        startService(msgIntent);
//    }
//
//    @Override
//    protected void onDestroy() {
//        unregisterReceiver(mReceiver);
//        super.onDestroy();
//    }
//
//    private void buildComponents() {
//        etName = (EditText) findViewById(R.id.et_name);
//        mUrlInputEditText = (EditText) findViewById(R.id.url_input_edittext);
//        mStartDownloadButton = (Button) findViewById(R.id.start_download_button);
//        mShowDownloadListButton = (Button) findViewById(R.id.show_download_list_button);
//
//        mStartDownloadButton.setOnClickListener(this);
//        mShowDownloadListButton.setOnClickListener(this);
//
//        mUrlInputEditText.setText("http://down.mumayi.com/41052/mbaidu");
//    }
//
//    private void startDownloadService() {
//        Intent intent = new Intent();
//        intent.setClass(this, DownloadService.class);
//        startService(intent);
//    }
//
//    @Override
//    public void onClick(View v) {
////        int id = v.getId();
////        switch (id) {
////            case R.id.start_download_button:
////                startDownload();
////                break;
////            case R.id.show_download_list_button:
////                showDownloadList();
////                break;
////            default:
////                break;
////        }
//    }
//
//    private void showDownloadList() {
//        Intent intent = new Intent();
//        intent.setClass(this, DownloadList.class);
//        startActivity(intent);
//    }
//
//    private void startDownload() {
//        String url = mUrlInputEditText.getText().toString();
//        Uri srcUri = Uri.parse(url);
//        DownloadManager.Request request = new Request(srcUri);
//        request.setDestinationInExternalPublicDir(
//                Environment.DIRECTORY_DOWNLOADS, "/");
//        request.setTitle("Just for test");
//        request.setDescription(etName.getText().toString());
//        long enqueue = mDownloadManager.enqueue(request);
//
//        DowloadApplication dowloadApplication = (DowloadApplication) getApplication();
//        dowloadApplication.setCurrentDownloadId(enqueue);
//
//        Intent intent = new Intent();
//        intent.setClass(this, ExapmleActivity.class);
//        startActivity(intent);
//    }
//}